## ----setup, include=FALSE------------------------------------------------
knitr::opts_chunk$set(echo = TRUE)


## ---- tidy=FALSE---------------------------------------------------------
dat1 = data.frame(name=c('Kim', 'Park'),
                  age=c(24, 40), 
                  height=c(180,165), stringsAsFactors=FALSE)
dat2 <- dat1


## ---- results='hold', tidy=FALSE-----------------------------------------
dat1[1,] <- c("Lee", 30, 170); dat1
dat2[1,] <- list("Lee", 30, 170); dat2


## ---- results='hold', tidy=FALSE-----------------------------------------
dat1$age
dat2$age


## ---- tidy=FALSE---------------------------------------------------------
dat2 = data.frame(name=c('Kim', 'Park'),
                  age=c(24, 40), 
                  height=c(180,165), stringsAsFactors=TRUE)
dat2[1,] <- list("Lee", 30, 170); dat2


## ---- tidy=FALSE---------------------------------------------------------
dat <- read.csv('서울시 한강공원 이용객 현황 (2009_2013년).csv', 
                fileEncoding = 'UTF-8-BOM')
head(dat, n=2)
# BOM
# http://blog.wystan.net/2007/08/18/bom-byte-order-mark-problem
# https://code.i-harness.com/ko-kr/q/21ef0a

## ------------------------------------------------------------------------
dat02 <- read.csv('서울특별시 공공자전거 대여소별 이용정보(월간)_2017_1_12.csv',
                  quote="'")
head(dat02, n=2)


## ---- tidy=FALSE---------------------------------------------------------
dat03 <- read.csv("http://www.nber.org/data/population-birthplace-diversi
ty/JoEG_BP_diversity_data.csv", sep=";", header=T, row.names=NULL)
head(dat03, n=2)

## ---- include=F, eval=F--------------------------------------------------
## dat03 <- read.csv("http://www.nber.org/data/population-birthplace-diversi
## ty/JoEG_BP_diversity_data.csv")
## head(dat03, n=2)


## ------------------------------------------------------------------------
library(readxl)
dat04 <- 
  readxl::read_xlsx('서울시 한강공원 이용객 현황 (2009_2013년).xls', sheet=1) 
head(dat04, n=3)


## ---- eval=F, tidy=FALSE-------------------------------------------------
## foreign::read.dta('http://www.nber.org/data/population-birthplace-diver
## sity/JoEG_BP_diversity_data.dta')
## haven::read_dta('http://www.nber.org/data/population-birthplace-diversi
## ty/JoEG_BP_diversity_data.dta')
## readstata13::read.dta13('http://www.nber.org/data/population-birthplace
## -diversity/JoEG_BP_diversity_data.dta')


## ----message=FALSE, tidy=FALSE-------------------------------------------
library(XML); library(RCurl); library(rlist)
url <- "https://en.wikipedia.org/wiki/List_of_Korean_surnames"
theurl <- getURL(url, .opts=list(ssl.verifypeer=FALSE))
df2 <- readHTMLTable(
  theurl, header=TRUE, which=1, 
  stringsAsFactors=FALSE, encoding='UTF-8')
head(df2, n=1)


## ---- eval=FALSE, tidy=FALSE---------------------------------------------
## switch(x,
##        "1"="one",
##        "2"="two",
##        "else!") # 숫자는 따옴표로 감싸야 함을 주의하자.

